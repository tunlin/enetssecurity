Pod::Spec.new do |s|

s.name         = "eNETSSecurity"
s.version      = "0.0.1"
s.summary      = "eNETS Helper"
s.description  = "eNETS Mobile SDK Helper"

s.homepage     = "http://nets.com.sg"
s.license      = "BSD"
# s.license    = { :type => "MIT", :file => "FILE_LICENSE" }

s.author       = { "enets team" => "info@nets.com.sg" }
s.platform     = :ios, "9.0"
s.ios.deployment_target = "9.0"

s.source     = { :git => "https://bitbucket.org/tunlin/enetssecurity", :tag => s.version.to_s }

s.source_files  = "eNETSSecurity/**/*.{h,m}", "include-ios/openssl/**/*.h"

s.public_header_files = 'include-ios/openssl/**/*.h'
s.header_dir          = 'openssl'
s.preserve_paths      = 'lib-ios/libcrypto.a', 'lib-ios/libssl.a'
s.vendored_libraries  = 'lib-ios/libcrypto.a', 'lib-ios/libssl.a'
s.libraries           = 'ssl', 'crypto'

s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(PROJECT_DIR)/include-ios" }
s.xcconfig = { "LIBRARY_SEARCH_PATHS" => "$(PROJECT_DIR)/NETSMobile" }


end
